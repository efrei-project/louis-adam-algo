import exo7 as exo


class Struct(exo.Struct):

    def __init__(self, *args):
        exo.Struct.__init__(self, *args)

    def min(self):
        if len(self.data) == 0:
            raise Exception

        cursor: int = None
        is_changed = False
        for elem in self.data:
            if isinstance(elem, int):
                cursor = elem if cursor is None else cursor
                print(f'elem: {elem} - cursor: {cursor}')
                if elem <= cursor:
                    cursor = elem
                    is_changed = True
            else:
                raise Exception
        if is_changed:
            return cursor
        else:
            raise Exception