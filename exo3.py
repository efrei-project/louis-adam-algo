def script(string, test):
    if not isinstance(string, str):
        raise Exception

    result = ""
    length = len(string)
    for char in string:
        result += string[length - 1]
        length -= 1

    return True if result == test else False
