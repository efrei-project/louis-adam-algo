import pytest
import exo10 as exo


def test_create_good():
    card = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "five")
    assert card.color.value == 2


def test_create_error_checking():
    with pytest.raises(Exception) as e:
        card = exo.Cards(exo.Color.RED, exo.Symbol.SPADE, "five")


def test_create_error_args():
    with pytest.raises(Exception) as e:
        card = exo.Cards("hello world", exo.Symbol.SPADE, "five")


def test_compare_color():
    card = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "two")
    card2 = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "five")
    card3 = exo.Cards(exo.Color.BLACK, exo.Symbol.SPADE, "five")
    assert card.is_same_color(card2) is True
    assert card.is_same_color(card3) is False


def test_compare_symbol():
    card = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "two")
    card2 = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "five")
    card3 = exo.Cards(exo.Color.BLACK, exo.Symbol.SPADE, "five")
    assert card.is_same_symbol(card2) is True
    assert card.is_same_symbol(card3) is False


def test_compare_value():
    card = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "two")
    card2 = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "five")
    card3 = exo.Cards(exo.Color.BLACK, exo.Symbol.SPADE, "two")
    assert card.is_same_value(card2) is False
    assert card.is_same_value(card3) is True


def value():
    card = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "two")
    card2 = exo.Cards(exo.Color.RED, exo.Symbol.DIAMOND, "five")
    assert card.min(card2) == card
    assert card.max(card2) == card2