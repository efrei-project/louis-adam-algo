import pytest
import exo9 as exo


def test_pop():
    my_struct = exo.Struct(1, 2, 3, 4, 5, 6)
    assert my_struct.min() == 1


def test_error():
    my_struct = exo.Struct()
    with pytest.raises(Exception) as e:
        my_struct.min()


def test_error_unchanged():
    my_struct = exo.Struct("test", "checkifwork")
    with pytest.raises(Exception) as e:
        my_struct.min()


def test_error_mix_data():
    my_struct = exo.Struct(124, "checkifwork")
    with pytest.raises(Exception) as e:
        my_struct.min()