import pytest
import exo3 as exo


def test_true():
    result = exo.script("salut bon", "nob tulas")
    assert result == True


def test_false():
    result = exo.script("coucou", "test")
    assert result == False


def test_struct():
    with pytest.raises(Exception) as e:
        result = exo.script(5)
