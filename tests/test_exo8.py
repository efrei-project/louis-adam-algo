import pytest
import exo8 as exo


def test_pop():
    my_struct = exo.Struct("test1", "hello", True, 124)
    assert my_struct.dequeue() == ["hello", True, 124]


def test_push_unique():
    my_struct = exo.Struct("test1", "hello", True, 124)
    result = my_struct.enqueue("thereisTest")
    assert result == ["test1", "hello", True, 124, "thereisTest"]


def test_push_multiple():
    my_struct = exo.Struct("test1", "hello", True, 124)
    result = my_struct.enqueue("thereisTest", "truc", False)
    assert result == ["test1", "hello", True, 124, "thereisTest", "truc", False]


def test_error():
    my_struct = exo.Struct("test1", "hello", True, 124)
    with pytest.raises(Exception) as e:
        length = len(my_struct.getData()) +1
        for i in range(0, length):
            my_struct.dequeue()
