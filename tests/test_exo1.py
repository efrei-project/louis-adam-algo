import pytest
import exo1 as exo


def test_true():
    result = exo.script("salut bon")
    assert result == True


def test_true():
    result = exo.script("a78bc def ghij klmn opq124rstu 6vwxyz")
    assert result == True


def test_false():
    result = exo.script("coucou")
    assert result == False


def test_struct():
    with pytest.raises(Exception) as e:
        result = exo.script(5)
