import pytest
import exo2 as exo


def test_true():
    result = exo.script("salut bon")
    assert result == "nob tulas"


def test_true_2():
    result = exo.script("coucou")
    assert result == "uocuoc"


def test_struct():
    with pytest.raises(Exception) as e:
        result = exo.script(5)
