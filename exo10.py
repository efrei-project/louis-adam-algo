from enum import Enum


class Color(Enum):
    BLACK = 1
    RED = 2


class Symbol(Enum):
    CLUB = 1 # pique
    DIAMOND = 2 # carreau
    SPADE = 3 # pique
    HEART = 4 # coeur


class Cards:

    def __init__(self, *args):
        try:
            color: Color = args[0]
            symbol: Symbol = args[1]
            value = args[2]
        except Exception as e:
            raise e

        if (color.value + symbol.value) % 2 != 0:
            raise Exception

        self.color = color
        self.symbol = symbol
        self.value = value

    def is_same_color(self, card):
        return True if self.color.value == card.color.value else False

    def is_same_symbol(self, card):
        return True if self.symbol.value == card.symbol.value else False

    def is_same_value(self, card):
        return True if self.value == card.value else False

    def min(self, card):
        self.value = 11 if self.value == "jack" else 12 if self.value == "queen" else 13 if self.value == "king" else self.value
        card.value = 11 if card.value == "jack" else 12 if card.value == "queen" else 13 if card.value == "king" else card.value
        return self if self.value < card.value  else card

    def max(self, card):
        self.value = 11 if self.value == "jack" else 12 if self.value == "queen" else 13 if self.value == "king" else self.value
        card.value = 11 if card.value == "jack" else 12 if card.value == "queen" else 13 if card.value == "king" else card.value
        return self if self.value > card.value  else card
