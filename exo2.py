def script(string):
    if not isinstance(string, str):
        raise Exception

    result = ""
    length = len(string)
    for char in string:
        result += string[length - 1]
        length -= 1

    return result
