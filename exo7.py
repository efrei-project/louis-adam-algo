class Struct:

    data = []

    def __init__(self, *args):
        self.data = []
        for arg in args:
            self.data.append(arg)

    def getData(self):
        return self.data

    def push(self, *args):
        for arg in args:
            self.data = self.data + [arg]
        return self.data

    def pop(self):
        length = len(self.data)
        if length == 0:
            raise Exception
        del self.data[length - 1]
        return self.data
