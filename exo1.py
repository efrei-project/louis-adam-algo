def script(string):
    if not isinstance(string, str):
        raise Exception

    for char in string:
        if char != " ":
            count = 0
            for cursor in string:
                if char == cursor:
                    count += 1
            if count > 1:
                return False
    return True
